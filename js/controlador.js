function logMessage() {
    console.log("Mensaje en consola");
}

function infoMessage() {
    console.info("Mensaje informativo");
}

function warningMessage() {
    console.warn("Mensaje de aviso");
}

function errorMessage() {
    console.error("Mensaje de error");
}

var bandera = 0;
/**
 * Sentencias de control
*/
function ifControl() {
    var count = 0;

    if ( count == 0 ) {
        count++;
    } else if ( count == 1 ) {
        count = count + 2;
    } else {
        count = 0;
    }

    console.log(count);
}

function whileControl() {
    var contabile = 0;

    while( contabile < 10 ) {
        console.log( "Contador: ", contabile );
        contabile++;
    }
}

function doWhileControl() {
    var kaunta = 0;

    do {
        console.log( "Contador en do While: ", kaunta );
        kaunta++;
    } while( kaunta < 5 );
}

function switchControl() {
    var comptable = 3;

    switch( comptable ) {
        case 0:
            console.log("Contador en 0");
            comptable++;
            break;

        case 1: 
            console.log("Contador en 1");
            comptable++;
            break;
        
        default:
            console.log("No hay valor :( ");
            break;
    }

}

function forControl() {

    for ( var i = 0; i < 5; i++ ) {
        console.log("Contador: ", i);
    }

    for ( var j = 5; j > 0; j-- ) {
        console.log("Contador regresivo: ", j);
    }
}